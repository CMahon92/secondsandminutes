package com.company;

public class Main {

    public static void main(String[] args) {
        getDurationString(90, 23);
        getDurationString(393);
    }
    public static int getDurationString (int minutes, int seconds) {
        boolean mins = (minutes >=0) ? true : false;
        boolean secs = (seconds >=0 && seconds <=59) ? true : false;

        if (mins && secs) {
            int hour = minutes / 60;
            int remainingMinutes = minutes % 60;
            System.out.println(hour+"h " + remainingMinutes + "m " + seconds + "s" );
            return hour;
        } else {
            System.out.println("Invalid Value");
            return -1;
        }
    }
    public static int getDurationString (int seconds) {
        if (seconds < 0) {
            System.out.println("Invalid Value");
            return -1;
        }
        int minsToSecs = seconds / 60;
        int remainingSeconds = minsToSecs % 60;
        return getDurationString(minsToSecs, remainingSeconds);
    }
}
